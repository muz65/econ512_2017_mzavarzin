% mzavarzin ha1 econ512

% 1.
X = [1,1.5,3,4,5,7,9,10];

Y1= -2 + X.*0.5;
Y2= -2 + X.^2*0.5;
plot(X,Y1,X,Y2);

% 2.
X = [-10:(30.0/199):20]';
length(X);

% 3.
% det(A)~=0;
% so we can compute A^-1

A = [2,4,6;1,7,5;3,12,4];
B = [-2;3;10];

C = A'*B;
D = (A'*A)\B;
E = ones(1,3)*C;
F = A(1:2:3,1:1:2);
x = A\B;

% 4.
% check out the kron() command
B = blkdiag(A,A,A,A,A);

% 5.
A = random('normal',10,5,5,3);
% here you need just A=A>10
A = A>=10*ones(5,3);

% 6.
% assume conditional mean independence of eps from the regressors
% (3), (4), (6); intercept is included in the model, so we can take
% conditional mean to be 0; assume data is iid

M = csvread('datahw1.csv',0,0);
Prod = M(:,5);
n = length(Prod);
n_adj = 1;
for i=2:n
    if M(i,1)~=M(i-1,1)
        n_adj = n_adj + 1;
    end
end
X = [ones(1,n);(M(:,3))';(M(:,4))';(M(:,6))'];

% define objective function
Q_hat = @(beta) (1/n)*(Prod-X'*beta)'*(Prod-X'*beta);

% solve for it numerically
% never use optimization when you know closed form solution
beta_hat1 = fminsearch(Q_hat,[1; 1; 1; 1]);

% or use the explicit solution (results are almost the same)
beta_hat2 =(X*X')\X*Prod;

% heteroscedastic standard errors--using asymptotic variance of OLS estimator
% n is taken to be the
err_hat = (Prod - X'*beta_hat2).^2;
var_hat = (X*X')\(X*diag(err_hat)*X')/(X*X');
std_err = diag(var_hat).^0.5;

% t-statistics (assuming beta_k0=0 under H0)
t_st = beta_hat2./std_err;

% p-values (assuming Ha: beta_k0<>0)
Pvalue = 2-2*cdf('normal',abs(t_st),0,1);






