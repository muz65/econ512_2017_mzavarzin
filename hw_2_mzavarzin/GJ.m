function [root,report] = GJ(f,x,a,m)
% solves for y s.t. f(y) = 0 using secant method on each sub-step
% root -- numerical root of non-linear d-to-d function
% report -- structure which shows algorithm steps

% f -- d-to-d function
% x -- d-by-1 initial guess vector
% a -- size of initial secant step
% m -- specify max amount of secant sub-steps

d = length(x);
x_copy = x;
max_iter=100;
tol = 1e-4; % set convergence criterion
tol = tol*(min(x)+a)/2; % make criterion depend on scale of problem

% outer loop in which x2 <- x1 is updated
for ni=1:max_iter
    root = zeros(1,d);
    
    % inner loop finds x2(d) <- x1(i) with x1(-i) const
    for i=1:d
        xd = @(y,pos) [x(1:pos-1);y;x(pos+1:end)];
        cut = zeros(1,d);
        cut([i]) = 1;
        fd = @(y) cut*f(xd(y,i)); % defines 1-to-1 function to be solved
        %root(i) = bisect(fd,x(d),x(d)+a);
        root(i) = SCT(fd,x(d),x(d)+a,m);
    end
    
    delta = abs(x-root');
    x = root';
    
    iter_root(ni,:) = root; % collect each step result
    iter_delta(ni,:) = max(delta);
    
    if delta <= tol
        break
    end    
end

if ni==max_iter
    display('convergence may not be achieved');
    display(delta, 'last delta');
    display(tol, 'tolerance level');
end

s.root = iter_root;
s.delta = iter_delta;
report = s;

return