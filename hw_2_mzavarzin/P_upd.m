function [root,report] = P_upd(q,x)
% q -- n-to-n demand function from the assignment (n -- number of firms)
% x -- n-by-1 initial guess vector

d = length(x);
x_copy = x;
max_iter=100;
tol = 1e-4;
tol = tol*(min(x))/2;

for ni=1:max_iter
    root = 1./(1-q(x));
    delta = abs(x-root);
    x = root;
    
    iter_root(ni,:) = root;
    iter_delta(ni,:) = max(delta);
    
    if delta <= tol
        break
    end    
end

if ni==max_iter
    display('convergence may not be achieved');
    display(delta, 'last delta');
    display(tol, 'tolerance level');
end

s.root = iter_root;
s.delta = iter_delta;
report = s;

return