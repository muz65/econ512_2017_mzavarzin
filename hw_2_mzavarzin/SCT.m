function [root,iter] = SCT(f,a,b,max_steps)
% finds solution to f(x) = 0 based on secant method; f(x) is 1D function
% root -- (numerical) root to f(x) = 0
% iter -- number of iterations until converged

% a,b -- initial values
% max_steps -- how many steps algorithm can make

max_iter=max_steps;
tol = 1e-5; % convergence criterion
tol = tol*0.5*(b-a); % make criterion depend on scale

% x_t+1 <- x_t,x_t-1
for ni=1:max_iter
    root = b-f(b)*((b-a)/(f(b)-f(a)));
    a = b;
    b = root;
    delta = abs(b-a);
    if delta < tol
        break
    end
end

iter = ni;

return
