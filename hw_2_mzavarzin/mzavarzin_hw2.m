%% Homework 2, Econ512
% M.Zavarzin
%
% Also please see attached functions -- broyden (from CE toolbox), GJ, SCT,
% and P_upd.
%%
clear;

q = @(p) exp(-1-p.*1)/(sum(exp(-1-p.*1))); % demand system
n = 3; % number of firms
q_f = @(p) [eye(n),zeros(n,1)]*q([p;-1]); % demand system w.o. outside option

%% 1.
% Demand for each option is given by:
ans1 = q([ones(n,1);-1])

%% 2.
% To find Bertrand equilibrium, we need to solve each firm's profit 
% maximisation problem;
% corresponding FOC of each firm gives an implicit best response function;
% thus, we have a system of nonlinear equations:
foc_system = @(p) ones(n,1)-diag(ones(n,1)-q_f(p))*p;
% I think you have multiplied FOC by (1-Q) and that made all the trouble
% with broyden
%%
% [I wanted to iterate over starting values, to make the code cleaner,
% but haven't figured out how.]
stv = [[1;1;1] [0;0;0] [0;1;2] [3;2;1]];

% use broyden.m from CE toolbox
[ans2a,fa,fla,it2a] = broyden(foc_system,stv(:,1));
[ans2b,fb,flb,it2b] = broyden(foc_system,[0;0;0]);
[ans2c,fc,flc,it2c] = broyden(foc_system,[0;1;2]);
[ans2d,fd,fld,it2d] = broyden(foc_system,[3;2;1]);
%%
% broyden.m has a tolerance level set at eps; it takes 4-7 iterations to
% converge. The numerical solution for NE vector of prices is always the
% same no matter what method or starting value we use, so we report it only
% once:
ans2a

%% 3.
% See GJ.m; it[..] shows the  steps of the algorithm.
[ans3a,it3a] = GJ(foc_system,[1;1;1],0.1,100);
[ans3b,it3b] = GJ(foc_system,[0;0;0],0.1,100);
[ans3c,it3c] = GJ(foc_system,[0;1;2],0.1,100);
[ans3d,it3d] = GJ(foc_system,[3;2;1],0.1,100);
% It's wired, but the system actually converges this time. 
%%
% Gauss-Jacobi method converges
% in 3-5 steps, and here only in a) Broyden's method is faster. The reason
% why G-J is taking less steps to converge could be the following. Here we expect own-partial
% derivatives to be the strongest (the price of your good affects your demand
% the strongest). So the G-J method which exploits exactly this effect is
% faster than the Broyden's method which approximates the whole Jacobian
% (cross-partial derivatives effects may slow down the convergence).
%% 4.
% See P_upd.m.
[ans4a,it4a] = P_upd(q_f,[1;1;1]);
[ans4b,it4b] = P_upd(q_f,[0;0;0]);
[ans4c,it4c] = P_upd(q_f,[1;1;1]);
[ans4d,it4d] = P_upd(q_f,[1;1;1]);
%%
% This update rule converges, but the number of steps varies dramatically
% from 5 to 16 in case of b) (which could be interpreted as the initial
% guess farthest from the truth). This could reflect somewhat random nature
% of the update rule. Although, the fact that starting from "any"
% out-of-equilibrium point we converge to NE when everybody follows their
% equilibrium strategy may seem surprising and can indicate "stability" of
% the Bertrand equilibrium.

%% 5. 
[ans5a,it5a] = GJ(foc_system,[1;1;1],0.1,1);
[ans5b,it5b] = GJ(foc_system,[0;0;0],0.1,1);
[ans5c,it5c] = GJ(foc_system,[0;1;2],0.1,1);
[ans5d,it5d] = GJ(foc_system,[3;2;1],0.1,1);
%%
% Notably, with a single step we have the same convergence rates (from 3 to
% 5). This indicates that own-partial derivative influence is so strong
% and convergence rates are so fast that we can be less precise in computing
% updates (where we use only own-partial derivative approximation).
%
% Some of the iterations:
it5a.root
it5b.root
it5c.root
it5d.root