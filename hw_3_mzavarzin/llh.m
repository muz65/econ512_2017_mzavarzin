function [LLH, grad_LLH]=llh(b,X,y)
    LLH=-sum(-exp(X*b)+y.*(X*b)-log(factorial(y)));
    grad_LLH=-(-X'*exp(X*b)+X'*y);
end