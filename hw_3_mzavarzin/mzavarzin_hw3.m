% econ512 ps3 mzavarzin
% MF42 - part A

clear;

% generate data and compute Y1 and Y2
n=1000;
th=[2;2];
x=random('Gamma',th(1),1/th(2),1,n); %in matlab second parameter is defined as inverse
Y_1=sum(x)/n;
Y_2=(prod(x))^(1/n);

% differentiating log-likelihood we get foc which we have to solve with
% newton's method
foc=@(th) -log(Y_1/Y_2)+log(th)-digamma(th);
dfoc=@(th) (1/th)-trigamma(th);

steps=500; %maximum number of newton steps
th1_0=1.5; %initial guess
toler=th(1)/10000; %level of error
th_old=th1_0;
for i=1:steps
    th1_MLE=th_old-foc(th_old)*(dfoc(th_old))^(-1);
    if abs(th1_MLE-th_old)<toler
        flg=1;
        break;
    end
    if abs(th1_MLE-th_old)>1/toler
        flg=2;
        th1_MLE=th1_MLE/100000;
    end
    th_old=th1_MLE;
end

if i==steps
    display('maximum number of steps used');
end

% MLE estimator of parameters
th_MLE=[th1_MLE; th1_MLE/Y_1];

% estimation of asymptotic variance of MLE estimator
score_hat=(1/n)*[-log(Y_1)+log(th_MLE(1))+...
    log(x)-digamma(th_MLE(1)); Y_1-x]*...
    [-log(Y_1)+log(th_MLE(1))+...
    log(x)-digamma(th_MLE(1)); Y_1-x]';
hessian_hat=(1/n)*n*[-trigamma(th_MLE(1)), 1/th_MLE(2);...
    1/th_MLE(2), -th_MLE(1)/(th_MLE(2)^2)];
V_hat=(hessian_hat\score_hat/hessian_hat);

% theta_1 as a function of y=Y_1/Y_2
f=@(y,th) -log(y)+log(th)-digamma(th);
k=1;
for y=1.1:0.05:3
    th_old=th1_0;
    
    for i=1:steps
        th1_y(k)=th_old-f(y,th_old)*(dfoc(th_old))^(-1);
        if abs(th1_y(k)-th_old)<toler
            flg=1;
            break;
        end
        if isnan(th1_y(k))
            flg=2;
            th1_y(k)=10*rand; %hard to guess better initial value; for other values of y, another search interval may be needed
        end
        th_old=th1_y(k);
    end
    
    k=k+1;
    if i==steps
    display(k,'maximum number of steps used');
    end
end

plot(1.1:0.05:3,th1_y)
    
