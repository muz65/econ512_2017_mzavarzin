% econ512 ps3 mzavarzin
% part B

clear;

% import data - contains matrix X (601-by-6) and y (601-by-1)
load('hw3.mat')
n=length(X(:,1));

% define log-likelihood and RSS
LLH=@(b) -sum(-exp(X*b)+y.*(X*b)-log(factorial(y)));
grad_LLH=@(b) -(-X'*exp(X*b)+X'*y); %k-by-1
score=@(b) X'*diag((-exp(X*b)+y).^2)*X; %k-by-k
resid=@(b) (y-exp(X*b)); %n-by-1
J_NLLS=@(b) -diag(exp(X*b))*X; %n-by-k
sc_NLLS=@(b) X'*diag(exp(X*b).^2)*X; %k-by-k
RSS=@(b) resid(b)'*resid(b);

b0=zeros(length(X(1,:)),1);

% fminunc w.o. supplied derivative
options1=optimoptions('fminunc','Algorithm','quasi-newton');
[b_fmu_nograd,fval1,exitflag1,output1]=fminunc(LLH,b0,options1);

% fminunc w. supplied derivative
options2=optimoptions('fminunc','Algorithm','trust-region','SpecifyObjectiveGradient',true);
fun=@(b) llh(b,X,y); %had to define a function and its gradient in a separate file
[b_fmu_grad,fval2,exitflag2,output2]=fminunc(fun,b0,options2);

% applying NM method to single-dimensional function
% starting value of zeroes produces likely incorrect estimates
[b_NM,fval3,exitflag3,output3]=fminsearch(LLH,b0);
% changing the starting value slightly leads to the same estimated
% parameter values as in other methods
% [b_NM,fval3,exitflag3,output3]=fminsearch(LLH,0.1*ones(6,1));

% BHHH method
steps=500;
toler=0.0001;
b_old=[0;0;0;0;0;0];
for i=1:steps
    b_new=b_old-score(b_old)\grad_LLH(b_old);
    if abs(b_new-b_old)<toler
        flg=1;
        break;
    end
    if isnan(b_new) %||((b_new-b_old)>1/toler)
        flg=2;
        b_new=5*rand;
    end
    b_old=b_new;
end

% NLLS
[b_NLLS,fval4,exitflag4,output4]=fminunc(RSS,b0,options1);

% Gauss-Newton method for NLLS
b_old2=[0;0;0;0;0;0];
for k=1:steps
    b_new2=b_old2-sc_NLLS(b_old2)\J_NLLS(b_old2)'*resid(b_old2);
    if abs(b_new2-b_old2)<toler
        flg2=1;
        break;
    end
    if isnan(b_new2) %||((b_new-b_old)>1/toler)
        flg2=2;
        b_new2=5*rand;
    end
    b_old2=b_new2;
end

a_var_NLLS=(sc_NLLS(b_new2)\J_NLLS(b_new2)'*resid(b_new2))*...
    (sc_NLLS(b_new2)\J_NLLS(b_new2)'*resid(b_new2))'; %standard errors seem too small here,maybe need to multiply by n

display(b_fmu_nograd','fminunc no derivative')
output1

display(b_fmu_grad','fminunc with derivative')
output2

display(b_NM','Nelder Mead')
output3

display(b_new','BHHH')
display(i,'iterations')

display(b_NLLS','NLLS')
output4

display(b_new2,'NLLS with Gauss-Newton')
display(k,'iterations')

display(eig(-score(b0))','eigenvalues of approximated Hessian at initial value')
display(eig(-score(b_new))','eigenvalues of approximated Hessian at estimated parameter value')

display((diag(inv(score(b_new)))/n).^(0.5)','standard errors of BHHH estimates')
display((diag(a_var_NLLS)/n).^(0.5)','standard errors of NLLS estimates')


