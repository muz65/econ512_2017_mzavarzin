function llh = LLH_1D(N,b0,s_b,g,LH_i,method)

% compute the log-likelihood function for given data and parameter values
% [b0--mean of beta_i, s_b--variance of beta_i, g--gamma]
% as well as corresponding individual likelihoods LH_i
% using methods from 1. and 2.

rng(2);
global x20 w20
x=x20;
w=w20;

if method==2 % MC method
    U=normrnd(b0,sqrt(s_b),100,100); % normrnd takes std dev as input
    int=zeros(1,N);
    for k=1:100
        int=int+(1/100)*LH_i(U(k,:),g,0);
    end
    llh=sum(log(int));
else % GQ method
    int=zeros(1,N);
    for k=1:20
        int=int+w(k)*LH_i(b0+sqrt(2*s_b)*x(k),g,0)/sqrt(pi);
    end
    llh=sum(log(int));
end