function [c, ceq] = constr_det(x)

% constraint: covariance matrix must be PSD; need to check that all
% principal minors are >=0; here check that det(x)>=0, or in fmincon syntax
% -det(x)<=0;
% x is a 2-by-2 matrix

ceq=[];
V=[x(3),x(4);x(4),x(5)]
c=-det([x(3),x(4);x(4),x(5)]);
end



