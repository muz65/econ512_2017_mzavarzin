% econ517 ha4 mzavarzin

clear;
load('hw4data.mat')
rng(2);

beta0=0.1;
sig_beta=1;

F=@(eps) (1+exp(-eps)).^(-1);

% compute 1-by-N vector of individual likelihoods
LH_i=@(b,g,u)...
    prod((F(data.X*diag(b)+data.Z*diag(g)+ones(20,100)*diag(u)).^data.Y).*...
    (1-F(data.X*diag(b)+data.Z*diag(g)+ones(20,100)*diag(u))).^(1-data.Y));

%% 1. compute the (log-) likelihood for u=0, gamma=0 using Gaussian quadrature
% compute quadrature nodes and weights
global x20 w20
% aviod using global. it does not allow you do parallel computations and in
% general bad habbit. 
[x20,w20]=GaussHermite_2(20);

% compute (approximate) integrals
int1=zeros(1,data.N);
for k=1:20
    int1=int1+w20(k)*LH_i(beta0+sqrt(2*sig_beta)*x20(k),0,0)/sqrt(pi);
end
% compute log-likelihood
LLH1=sum(log(int1));
%LH=exp(LLH); 

%% 2. compute the (log-) likelihood using MC methods with 100 draws
% here we make a transformation U_i=1/(1+exp(-beta_i)), as we want
% the limits of integration to be 0 and 1

% vector of 100*100 draws--get 100 nodes for each integral_i, i=1,..,100
U=normrnd(beta0,sqrt(sig_beta),100,100); % normrnd takes std dev as input
% compute the integrals
int2=zeros(1,data.N);
for k=1:100
    int2=int2+(1/100)*LH_i(U(k,:),0,0);
end
% and the log-likelihood is
LLH2=sum(log(int2));

%% 3. maximise the likelihood function and estimate parameters
% set up the above two methods as a function LLH_1D
st=[2;1;0];
fun1=@(x) -LLH_1D(data.N,x(1),x(2),x(3),LH_i,1); % compute with GQ method
[est1,f1]=fmincon(fun1,st,diag([0,-1,0]),zeros(3,1));

fun2=@(x) -LLH_1D(data.N,x(1),x(2),x(3),LH_i,2); % MC method
[est2,f2]=fmincon(fun2,st,diag([0,-1,0]),zeros(3,1));

%% 4. allow for joint normal distribution
fun3=@(x) -LLH_2D(data.N,x(1),x(2),x(3),x(4),x(5),x(6),LH_i);

% we need to check that cov matrix is PSD; linear constraint checks
% individual variances >=0; nonlinear constraint checks det(V)>=0
st2=[2;1;2;1;1;-0.5];
A=diag([0,0,-1,0,-1,0]);
b=zeros(6,1);
[est3,f3]=fmincon(fun3,st2,A,b,[],[],[],[],@constr_det);
%options = optimoptions(@fminunc,'Algorithm','quasi-newton');
%[est3a,f3a]=fminunc(fun3,st2,options);

%% 5. display results
display(LLH1,'log-likelihood computed with GQ 20')
display(LLH2,'log-likelihood computed with MC 100')
display(st,'starting value for estimation in 3.')
display(est1,'GQ 20 estimates of beta0,sigma_beta,gamma')
display(f1,'GQ 20 maximised value of LLH')
display(est2,'MC 100 estimates of beta0,sigma_beta,gamma')
display(f2,'MC 100 maximised value of LLH')
display(st2,'starting value for estimation in 4.')
display(est3,'MC 100 estimates of beta0,u0,s_b,s_bu,su,gamma')
display(f3,'MC 100 maximised value of LLH')
